/**
 * Array based stack.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_STACK_H
#define ALGC_STACK_H

#include "common.h"

typedef struct steck steck_t;

/**
 * Allocate a new stack.
 * @param size size of items to be stored.
 * @return     pointer to the new stack allocated.
 */
steck_t* steck_alloc(size_t size);

/**
 * Deallocate stack.
 * @param steck stack to be deallocated.
 */
void steck_free(steck_t* stack);

/**
 * Push item to the stack provided.
 * @param steck stack to be updated.
 * @param value item to be pushed.
 */
void steck_push(steck_t* stack, void* value);

/**
 * Pop item from the stack provided.
 * @param  steck stack to be updated.
 * @return       item from top of the stack.
 */
bool steck_pop(steck_t* stack, void** value);

/**
 * Get item from top of the stack provided without it removing.
 * @param  steck the stack.
 * @return       value from top of the stack.
 */
void* steck_top(steck_t* stack);

/**
 * Get count of items in the stack provided.
 * @param  steck the stack.
 * @return       count of items in the stack provided.
 */
size_t steck_count(steck_t* steck);

#endif // !ALGC_STACK_H
