#include "stack.h"

#include <string.h>

static const int INITIAL_LEN = 4;
static const int SCALE_FACTOR = 2;

struct steck
{
    intptr_t head;
    intptr_t tail;
    size_t size;
    size_t len;
};

static void steck_realloc(steck_t* steck, size_t len)
{
    size_t diff = steck->tail - steck->head;
    steck->head = (intptr_t)realloc((void*)steck->head, len);
    steck->tail = steck->head + diff;
    steck->len = len;
}

steck_t* steck_alloc(size_t size)
{
    steck_t* steck = (steck_t*)malloc(sizeof(steck_t));

    steck->size = size;
    steck->len = INITIAL_LEN * steck->size;
    steck->head = (intptr_t)malloc(steck->len);
    steck->tail = steck->head;

    return steck;
}

void steck_free(steck_t* steck)
{
    free((void*)steck->head);
    free(steck);
}

void steck_push(steck_t* steck, void* value)
{
    if (steck->tail - steck->head == steck->len)
    {
        steck_realloc(steck, steck->len * SCALE_FACTOR);
    }

    memcpy((void*)steck->tail, &value, steck->size);
    steck->tail += steck->size;
}

bool steck_pop(steck_t* steck, void** value)
{
    if (steck->tail == steck->head)
    {
        return false;
    }

    if (steck->tail - steck->head == steck->len / (SCALE_FACTOR * 2))
    {
        steck_realloc(steck, steck->len / SCALE_FACTOR);
    }

    steck->tail -= steck->size;
    memcpy(value, (void*)steck->tail, steck->size);

    return true;
}

void* steck_top(steck_t* steck)
{
    return steck->tail != steck->head ? (void*)(steck->tail - steck->size)
                                      : NULL;
}

size_t steck_count(steck_t* steck)
{
    return (steck->tail - steck->head) / steck->size;
}
