/**
 * Priority queue.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_HEAP_H
#define ALGC_HEAP_H

#include "common.h"

typedef struct heap heap_t;

/**
 * Allocate new heap.
 * @param size heap size.
 * @param cmp  items comparator.
 */
heap_t* heap_alloc(size_t size, int (*cmp)(const void*, const void*));

/**
 * Deallocate the heap provided.
 * @param heap the heap to be deallocated.
 */
void heap_free(heap_t* heap);

/**
 * Insert item into heap.
 * @param heap the heap where item should be inserted.
 * @param item the item to be inserted.
 */
void heap_push(heap_t* heap, void* item);

/**
 * Remove and return the item with the max priority.
 * @param heap the heap.
 */
int heap_pop(heap_t* heap, void** item);

/**
 * Get the item with the max priority.
 * @param heap the heap.
 */
void* heap_top(heap_t* heap);

/**
 * Get count of items in the heap provided.
 * @param  heap the heap to exemine.
 * @return      count of items in the heap.
 */
size_t heap_count(heap_t* heap);

#endif //! ALGC_HEAP_H