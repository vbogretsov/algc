#include "heap.h"

#include <stdlib.h>
#include <string.h>

#define swap(a, b, size)                                                       \
    do                                                                         \
    {                                                                          \
        size_t __size = (size);                                                \
        char *__a = (a), *__b = (b);                                           \
        do                                                                     \
        {                                                                      \
            char __tmp = *__a;                                                 \
            *__a++ = *__b;                                                     \
            *__b++ = __tmp;                                                    \
        } while (--__size > 0);                                                \
    } while (0)

static const int INITIAL_LENGTH = 4;
static const int SCALE_FACTOR = 2;

struct heap
{
    intptr_t head;
    intptr_t tail;
    size_t size;
    size_t len;
    int (*cmp)(const void*, const void*);
};

static void heap_realloc(heap_t* heap, size_t len)
{
    intptr_t diff = heap->tail - heap->head;
    heap->head = (intptr_t)realloc((void*)heap->head, len);
    heap->tail = heap->head + diff;
    heap->len = len;
}

static void heap_swim(heap_t* heap)
{
    size_t k = (heap->tail - heap->head) / heap->size;

    intptr_t new = heap->tail - heap->size;
    intptr_t par = new - (k + 1) / 2 * heap->size;

    while (new > heap->head && heap->cmp((void*)new, (void*)par) > 0)
    {
        swap((void*)new, (void*)par, heap->size);
        new = par;
        k /= 2;
        par = new - (k + 1) / 2 * heap->size;
    }
}

static void heap_sink(heap_t* heap)
{
    intptr_t key = heap->head;
    while (key + (key - heap->head) + heap->size < heap->tail)
    {
        intptr_t j = key + (key - heap->head) + heap->size;
        if (j < heap->tail - heap->size &&
            heap->cmp((void*)j, (void*)(j + heap->size)) < 0)
        {
            j += heap->size;
        }
        if (heap->cmp((void*)key, (void*)j) > 0)
        {
            break;
        }
        swap((void*)key, (void*)j, heap->size);
        key = j;
    }
}

heap_t* heap_alloc(size_t size, int (*cmp)(const void*, const void*))
{
    heap_t* heap = (heap_t*)malloc(sizeof(heap_t));

    heap->len = INITIAL_LENGTH * size;
    heap->head = (intptr_t)malloc(heap->len);
    heap->tail = heap->head;
    heap->size = size;
    heap->cmp = cmp;

    return heap;
}

void heap_free(heap_t* heap)
{
    free((void*)heap->head);
    free(heap);
}

void heap_push(heap_t* heap, void* item)
{
    if (heap->tail - heap->head == heap->len)
    {
        heap_realloc(heap, heap->len * SCALE_FACTOR);
    }

    memcpy((void*)heap->tail, &item, heap->size);
    heap->tail += heap->size;
    heap_swim(heap);
}

int heap_pop(heap_t* heap, void** item)
{
    if (heap->head == heap->tail)
    {
        return false;
    }

    size_t half = heap->len / SCALE_FACTOR;
    if (heap->tail - heap->size == half / 2)
    {
        heap_realloc(heap, half);
    }

    heap->tail -= heap->size;
    swap((void*)heap->head, (void*)heap->tail, heap->size);
    memcpy(item, (void*)heap->tail, heap->size);
    heap_sink(heap);

    return true;
}

void* heap_top(heap_t* heap)
{
    return heap->tail != heap->head ? (void*)heap->head : NULL;
}

size_t heap_count(heap_t* heap)
{
    return (heap->tail - heap->head) / heap->size;
}
