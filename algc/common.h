/**
 * Common definitions for the algc library.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#endif // !COMMON_H
