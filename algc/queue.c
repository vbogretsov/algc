#include "queue.h"

#include <string.h>

static const int INITIAL_LEN = 4;
static const int SCALE_FACTOR = 2;

struct queue
{
    intptr_t data;
    intptr_t head;
    intptr_t tail;
    size_t count;
    size_t size;
    size_t len;
};

static inline intptr_t advance(intptr_t ptr, queue_t* queue)
{
    intptr_t end = queue->data + queue->len - queue->size;
    return ptr < end ? ptr + queue->size : queue->data;
}

static void queue_realloc(queue_t* queue, size_t len)
{
    intptr_t head = (intptr_t)malloc(len);
    intptr_t tail = head;

    do
    {
        // TODO: consider realloc.
        memcpy((void*)tail, (void*)queue->head, queue->size);
        queue->head = advance(queue->head, queue);
        tail += queue->size;
    } while (queue->head != queue->tail);

    free((void*)queue->data);

    queue->data = head;
    queue->head = head;
    queue->tail = tail;
    queue->len = len;

}

queue_t* queue_alloc(size_t size)
{
    queue_t* queue = (queue_t*)malloc(sizeof(queue_t));

    queue->size = size;
    queue->len = INITIAL_LEN * queue->size;
    queue->data = (intptr_t)malloc(queue->len);
    queue->head = queue->data;
    queue->tail = queue->data;
    queue->count = 0;

    return queue;
}

void queue_free(queue_t* queue)
{
    free((void*)queue->data);
    free(queue);
}

void queue_push(queue_t* queue, void* value)
{
    if (queue->count == queue->len)
    {
        queue_realloc(queue, queue->len * SCALE_FACTOR);
    }

    memcpy((void*)queue->tail, &value, queue->size);

    queue->tail = advance(queue->tail, queue);
    queue->count += queue->size;
}

bool queue_pop(queue_t* queue, void** value)
{
    if (queue->count == 0)
    {
        return false;
    }

    if (queue->count == queue->len / (SCALE_FACTOR * 2))
    {
        queue_realloc(queue, queue->len / SCALE_FACTOR);
    }

    memcpy(value, (void*)queue->head, queue->size);

    queue->head = advance(queue->head, queue);
    queue->count -= queue->size;

    return true;
}

void* queue_top(queue_t* queue)
{
    return queue->count > 0 ? (void*)queue->head : NULL;
}

size_t queue_count(queue_t* queue)
{
    return queue->count / queue->size;
}
