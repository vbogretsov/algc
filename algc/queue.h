/**
 * Array based queue.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_QUEUE_H
#define ALGC_QUEUE_H

#include "common.h"

typedef struct queue queue_t;

/**
 * Allocate a new queue.
 * @param  size size of items to be stored.
 * @return      pointer to the new queue allocated.
 */
queue_t* queue_alloc(size_t size);

/**
 * Deallocate queue.
 * @param queue queue to be deallocated.
 */
void queue_free(queue_t* queue);

/**
 * Push item to the queue provided.
 * @param queue queue to be updated.
 * @param value item to be pushed.
 */
void queue_push(queue_t* queue, void* value);

/**
 * Pop item from the queue provided.
 * @param  queue queue to be updated.
 * @return       item from top of the queue.
 */
bool queue_pop(queue_t* queue, void** value);

/**
 * Get item from top of the queue provided without it removing.
 * @param  queue the queue.
 * @return       value from top of the queue.
 */
void* queue_top(queue_t* queue);

/**
 * Get count of items in the queue provided.
 * @param  queue the queue.
 * @return       count of items in the queue provided.
 */
size_t queue_count(queue_t* queue);

#endif // !ALGC_QUEUE_H
