#include "test_heap.h"

#include <algc/heap.h>

static const int INITIAL_LENGTH = 4;
static const int SCALE_FACTOR = 2;

heap_t* test_heap;
int16_t max_item;

static int16_t* get_unordered_array(size_t len)
{
    int16_t* array = (int16_t*)malloc(len * sizeof(int16_t));
    for (int16_t i = 0; i < len; ++i)
    {
        array[i] = i;
    }
    shuffle(array, len, sizeof(int16_t));
    return array;
}

static void push_array_to_heap(heap_t* heap, int16_t* array, size_t len)
{
    for (size_t i = 0; i < len; ++i)
    {
        heap_push(heap, (void*)array[i]);
    }
}

static void pop_heap_to_array(heap_t* heap, int16_t* array, size_t len)
{
    int16_t item;
    for (size_t i = 0; i < len; ++i)
    {
        heap_pop(heap, (void*)&item);
        array[i] = item;
    }
}

static void assert_array_ordered(int16_t* array, size_t len)
{
    for (size_t i = 0; i < len - 1; ++i)
    {
        ck_assert_int_gt(array[i], array[i + 1]);
    }
}

static int int16cmp_asc(const void* a, const void* b)
{
    return *(int16_t*)a - *(int16_t*)b;
}

static void setup_new()
{
    test_heap = heap_alloc(sizeof(int16_t), &int16cmp_asc);
}

static void setup_reallocated_on_push()
{
    setup_new();

    for (int i = 0; i < INITIAL_LENGTH + 1; ++i)
    {
        heap_push(test_heap, (void*)i);
    }
    max_item = INITIAL_LENGTH;
}

static void setup_reallocated_on_pop()
{
    setup_reallocated_on_push();

    int16_t item;
    for (int i = 0; i < INITIAL_LENGTH / SCALE_FACTOR + 1; ++i)
    {
        heap_pop(test_heap, (void*)&item);
    }

    max_item = 1;
}

static void setup_cleared()
{
    setup_reallocated_on_pop();

    int16_t item;
    while (heap_pop(test_heap, (void*)&item))
    {
    }
}

static void cleanup()
{
    heap_free(test_heap);
}

START_TEST(test_heap_push)
{
    heap_push(test_heap, (void*)10);
}
END_TEST

START_TEST(test_heap_pop_returns_max)
{
    int16_t item;
    ck_assert_int_eq(heap_pop(test_heap, (void*)&item), true);
    ck_assert_int_eq(item, max_item);
}
END_TEST

START_TEST(test_heap_pop_for_empty_heap_returns_false)
{
    int16_t item;
    ck_assert_int_eq(heap_pop(test_heap, (void*)&item), false);
}
END_TEST

START_TEST(test_heap_top_returns_max)
{
    int16_t item = *(int16_t*)heap_top(test_heap);
    ck_assert_int_eq(item, max_item);
}
END_TEST

START_TEST(test_heap_top_for_empty_heap_returns_null)
{
    ck_assert_ptr_eq(heap_top(test_heap), NULL);
}
END_TEST

START_TEST(test_heap_count_for_empty_heap_returns_zero)
{
    ck_assert_int_eq(heap_count(test_heap), 0);
}
END_TEST

START_TEST(test_heap_count_increased_after_push)
{
    size_t before = heap_count(test_heap);
    heap_push(test_heap, (void*)10);

    size_t after = heap_count(test_heap);
    ck_assert_int_eq(before + 1, after);
}
END_TEST

START_TEST(test_heap_count_decreased_after_pop)
{
    heap_push(test_heap, (void*)10);
    size_t before = heap_count(test_heap);

    int16_t item;
    heap_pop(test_heap, (void*)&item);

    size_t after = heap_count(test_heap);
    ck_assert_int_eq(before - 1, after);
}
END_TEST

START_TEST(test_heap_orders_random_array)
{
    size_t test_array_len = abs(rand()) % 1000;

    int16_t* test_array = get_unordered_array(test_array_len);

    push_array_to_heap(test_heap, test_array, test_array_len);
    pop_heap_to_array(test_heap, test_array, test_array_len);

    assert_array_ordered(test_array, test_array_len);

    free(test_array);
}
END_TEST

static tcase_t* heap_tcase_new()
{
    tcase_t* tcase = tcase_create("test_heap_new");
    tcase_add_checked_fixture(tcase, setup_new, cleanup);

    tcase_add_test(tcase, test_heap_push);
    tcase_add_test(tcase, test_heap_pop_for_empty_heap_returns_false);
    tcase_add_test(tcase, test_heap_top_for_empty_heap_returns_null);
    tcase_add_test(tcase, test_heap_count_for_empty_heap_returns_zero);
    tcase_add_test(tcase, test_heap_count_increased_after_push);
    tcase_add_test(tcase, test_heap_count_decreased_after_pop);
    tcase_add_test(tcase, test_heap_orders_random_array);

    return tcase;
}

static tcase_t* heap_tcase_reallocated_on_push()
{
    tcase_t* tcase = tcase_create("test_heap_reallocated_on_push");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_push, cleanup);

    tcase_add_test(tcase, test_heap_push);
    tcase_add_test(tcase, test_heap_pop_returns_max);
    tcase_add_test(tcase, test_heap_top_returns_max);
    tcase_add_test(tcase, test_heap_count_increased_after_push);
    tcase_add_test(tcase, test_heap_count_decreased_after_pop);

    return tcase;
}

static tcase_t* heap_tcase_reallocated_on_pop()
{
    tcase_t* tcase = tcase_create("test_heap_reallocated_on_pop");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_pop, cleanup);

    tcase_add_test(tcase, test_heap_push);
    tcase_add_test(tcase, test_heap_pop_returns_max);
    tcase_add_test(tcase, test_heap_top_returns_max);
    tcase_add_test(tcase, test_heap_count_increased_after_push);
    tcase_add_test(tcase, test_heap_count_decreased_after_pop);

    return tcase;
}

static tcase_t* heap_tcase_cleared()
{
    tcase_t* tcase = tcase_create("test_heap_cleared");
    tcase_add_checked_fixture(tcase, setup_new, cleanup);

    tcase_add_test(tcase, test_heap_push);
    tcase_add_test(tcase, test_heap_pop_for_empty_heap_returns_false);
    tcase_add_test(tcase, test_heap_top_for_empty_heap_returns_null);
    tcase_add_test(tcase, test_heap_count_for_empty_heap_returns_zero);
    tcase_add_test(tcase, test_heap_count_increased_after_push);
    tcase_add_test(tcase, test_heap_count_decreased_after_pop);
    tcase_add_test(tcase, test_heap_orders_random_array);

    return tcase;
}

void add_heap_cases(suite_t* suite)
{
    suite_add_tcase(suite, heap_tcase_new());
    suite_add_tcase(suite, heap_tcase_reallocated_on_push());
    suite_add_tcase(suite, heap_tcase_reallocated_on_pop());
    suite_add_tcase(suite, heap_tcase_cleared());
}
