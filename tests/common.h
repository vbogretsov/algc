/**
 * Common definitions for tests.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_TEST_COMMON_H
#define ALGC_TEST_COMMON_H

#include <stdlib.h>

#include <check.h>

typedef TCase tcase_t;
typedef Suite suite_t;
typedef SRunner srunner_t;

/**
 * Random shuffle array.
 * @param array the array to be shuffled.
 * @param len   array length.
 * @param size  size of array's item.
 */
void shuffle(void* array, size_t len, size_t size);

#endif //! ALGC_TEST_COMMON_H