/**
 * Test cases for <algc/hashmap.h>.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_TEST_HASHMAP_H
#define ALGC_TEST_HASHMAP_H

#include "common.h"

void add_hashmap_cases(suite_t* suite);

#endif // !ALGC_TEST_HASHMAP_H