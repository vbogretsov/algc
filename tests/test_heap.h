/**
 * Test cases for <algc/heap.h>.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_TEST_HEAP_H
#define ALGC_TEST_HEAP_H

#include "common.h"

void add_heap_cases(suite_t* suite);

#endif // !ALGC_TEST_HEAP_H