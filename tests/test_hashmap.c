#include "test_hashmap.h"

#include <string.h>
#include <algc/hashmap.h>

static const int INITIAL_LENGTH = 4;
static const int SCALE_FACTOR = 2;

hashmap_t* test_map;

static int string_hash(const void* string)
{
    int hash = 1;

    for (char const* p = string; p != NULL && *p != '\0'; ++p)
    {
        hash = (hash << 1) ^ *p;
    }

    return hash;
}

static bool string_equals(const void* a, const void* b)
{
    return strcmp(a, b) == 0;
}

static void setup_new()
{
    test_map = hashmap_alloc(&string_hash, &string_equals);
}

static void setup_reallocated()
{
    setup_new();

    hashmap_set(test_map, "k1", "data1");
    hashmap_set(test_map, "k2", "data2");
    hashmap_set(test_map, "k3", "data3");
    hashmap_set(test_map, "k4", "data4");
}

static void setup_empty()
{
    setup_reallocated();

    hashmap_remove(test_map, "k1");
    hashmap_remove(test_map, "k2");
    hashmap_remove(test_map, "k3");
    hashmap_remove(test_map, "k4");
}

static void cleanup()
{
    hashmap_free(test_map);
}

START_TEST(test_hashmap_set_success)
{
    hashmap_set(test_map, "x1", "data1");
}
END_TEST

START_TEST(test_hashmap_set_overrides_data_if_key_exists)
{
    hashmap_set(test_map, "x1", "data1");
    hashmap_set(test_map, "x1", "data2");

    void* data;
    ck_assert(hashmap_get(test_map, "x1", &data));
    ck_assert(strcmp("data2", (const char*)data) == 0);
}
END_TEST

START_TEST(test_hashmap_get_sets_item_and_returns_true_if_key_exists)
{
    hashmap_set(test_map, "x1", "data1");
    hashmap_set(test_map, "x2", "data2");

    void* data;
    ck_assert(hashmap_get(test_map, "x1", &data));
    ck_assert(strcmp("data1", (const char*)data) == 0);


}
END_TEST

START_TEST(test_hashmap_get_returns_false_if_key_not_found)
{
    void* data;
    ck_assert(!hashmap_get(test_map, "x1", &data));
}
END_TEST

START_TEST(test_hashmap_remove_removes_key_end_returns_true_if_key_removed)
{
    hashmap_set(test_map, "x1", "data1");
    hashmap_set(test_map, "x2", "data2");

    ck_assert(hashmap_remove(test_map, "x1"));

    void* data;
    ck_assert(hashmap_get(test_map, "x2", &data));
}
END_TEST

START_TEST(test_hashmap_remove_returns_false_if_key_not_found)
{
    hashmap_set(test_map, "x1", "data1");
    hashmap_set(test_map, "x1", "data2");

    ck_assert(!hashmap_remove(test_map, "x3"));
}
END_TEST

START_TEST(test_hashmap_size_incremented_after_set)
{
    size_t s1 = hashmap_size(test_map);
    hashmap_set(test_map, "x1", "data1");
    size_t s2 = hashmap_size(test_map);
    ck_assert(s2 - s1 == 1);
}
END_TEST

START_TEST(test_hashmap_size_not_incremented_after_set_if_key_exists)
{
    hashmap_set(test_map, "x1", "data1");
    size_t s1 = hashmap_size(test_map);
    hashmap_set(test_map, "x1", "data1");
    size_t s2 = hashmap_size(test_map);
    ck_assert(s2 - s1 == 0);
}
END_TEST

START_TEST(test_hashmap_size_decremented_after_remove)
{
    hashmap_set(test_map, "x1", "data1");
    size_t s1 = hashmap_size(test_map);
    hashmap_remove(test_map, "x1");
    size_t s2 = hashmap_size(test_map);
    ck_assert(s1 - s2 == 1);
}
END_TEST

START_TEST(test_hashmap_size_not_decremented_after_remove_if_key_not_found)
{
    hashmap_set(test_map, "x1", "data1");
    size_t s1 = hashmap_size(test_map);
    hashmap_remove(test_map, "x2");
    size_t s2 = hashmap_size(test_map);
    ck_assert(s1 - s2 == 0);
}
END_TEST

START_TEST(test_hashmap_size_returns_zero_for_empty_container)
{
    ck_assert(hashmap_size(test_map) == 0);
}
END_TEST

static tcase_t* hashmap_tcase_new()
{
    tcase_t* tcase = tcase_create("test_hashmap_new");
    tcase_add_checked_fixture(tcase, setup_new, cleanup);

    tcase_add_test(tcase, test_hashmap_set_success);
    tcase_add_test(tcase, test_hashmap_set_overrides_data_if_key_exists);
    tcase_add_test(
        tcase,
        test_hashmap_get_sets_item_and_returns_true_if_key_exists);
    tcase_add_test(tcase, test_hashmap_get_returns_false_if_key_not_found);
    tcase_add_test(
        tcase,
        test_hashmap_remove_removes_key_end_returns_true_if_key_removed);
    tcase_add_test(tcase, test_hashmap_remove_returns_false_if_key_not_found);
    tcase_add_test(tcase, test_hashmap_size_incremented_after_set);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_incremented_after_set_if_key_exists);
    tcase_add_test(tcase, test_hashmap_size_decremented_after_remove);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_decremented_after_remove_if_key_not_found);
    tcase_add_test(tcase, test_hashmap_size_returns_zero_for_empty_container);

    return tcase;
}

static tcase_t* hashmap_tcase_reallocated()
{
    tcase_t* tcase = tcase_create("test_hashmap_reallocated");
    tcase_add_checked_fixture(tcase, setup_reallocated, cleanup);

    tcase_add_test(tcase, test_hashmap_set_success);
    tcase_add_test(tcase, test_hashmap_set_overrides_data_if_key_exists);
    tcase_add_test(
        tcase,
        test_hashmap_get_sets_item_and_returns_true_if_key_exists);
    tcase_add_test(tcase, test_hashmap_get_returns_false_if_key_not_found);
    tcase_add_test(
        tcase,
        test_hashmap_remove_removes_key_end_returns_true_if_key_removed);
    tcase_add_test(tcase, test_hashmap_remove_returns_false_if_key_not_found);
    tcase_add_test(tcase, test_hashmap_size_incremented_after_set);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_incremented_after_set_if_key_exists);
    tcase_add_test(tcase, test_hashmap_size_decremented_after_remove);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_decremented_after_remove_if_key_not_found);

    return tcase;
}

static tcase_t* hashmap_tcase_empty()
{
    tcase_t* tcase = tcase_create("test_hashmap_empty");
    tcase_add_checked_fixture(tcase, setup_empty, cleanup);

    tcase_add_test(tcase, test_hashmap_set_success);
    tcase_add_test(tcase, test_hashmap_set_overrides_data_if_key_exists);
    tcase_add_test(
        tcase,
        test_hashmap_get_sets_item_and_returns_true_if_key_exists);
    tcase_add_test(tcase, test_hashmap_get_returns_false_if_key_not_found);
    tcase_add_test(
        tcase,
        test_hashmap_remove_removes_key_end_returns_true_if_key_removed);
    tcase_add_test(tcase, test_hashmap_remove_returns_false_if_key_not_found);
    tcase_add_test(tcase, test_hashmap_size_incremented_after_set);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_incremented_after_set_if_key_exists);
    tcase_add_test(tcase, test_hashmap_size_decremented_after_remove);
    tcase_add_test(
        tcase,
        test_hashmap_size_not_decremented_after_remove_if_key_not_found);
    tcase_add_test(tcase, test_hashmap_size_returns_zero_for_empty_container);

    return tcase;
}

void add_hashmap_cases(suite_t* suite)
{
    suite_add_tcase(suite, hashmap_tcase_new());
    suite_add_tcase(suite, hashmap_tcase_reallocated());
    suite_add_tcase(suite, hashmap_tcase_empty());
}