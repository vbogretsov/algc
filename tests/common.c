#include "common.h"

#define swap(a, b, size)                                                       \
    do                                                                         \
    {                                                                          \
        size_t __size = (size);                                                \
        char *__a = (a), *__b = (b);                                           \
        do                                                                     \
        {                                                                      \
            char __tmp = *__a;                                                 \
            *__a++ = *__b;                                                     \
            *__b++ = __tmp;                                                    \
        } while (--__size > 0);                                                \
    } while (0)

void shuffle(void* array, size_t len, size_t size)
{
    size_t number_of_permutations = abs(rand()) % len;

    for (size_t i = 0; i < number_of_permutations; ++i)
    {
        size_t a = (abs(rand()) % len) * size;
        size_t b = (abs(rand()) % len) * size;

        swap(array + a, array + b, size);
    }
}