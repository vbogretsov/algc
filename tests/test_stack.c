#include "test_stack.h"

#include <algc/stack.h>

static const size_t INITIAL_LENGTH = 4;
static const size_t SCALE_FACTOR = 2;

steck_t* test_stack;

void setup_new()
{
    test_stack = steck_alloc(sizeof(int16_t));
}

void setup_reallocated_on_push()
{
    setup_new();

    for (int16_t i = 0; i < INITIAL_LENGTH + 1; ++i)
    {
        steck_push(test_stack, (void*)i);
    }
}

void setup_reallocated_on_pop()
{
    setup_reallocated_on_push();

    int16_t item;
    for (int i = 0; i < INITIAL_LENGTH / SCALE_FACTOR + 1; ++i)
    {
        steck_pop(test_stack, (void*)&item);
    }
}

void setup_cleared()
{
    setup_reallocated_on_push();

    int16_t item;
    while (steck_pop(test_stack, (void*)&item))
    {
    }
}

void cleanup()
{
    free(test_stack);
}

START_TEST(test_stack_push)
{
    steck_push(test_stack, (void*)10);
}
END_TEST

START_TEST(test_stack_pop_for_empty_returns_false)
{
    int16_t item;
    ck_assert_int_eq(steck_pop(test_stack, (void*)&item), false);
}
END_TEST

START_TEST(test_stack_pop_returns_last)
{
    int16_t expected = 10;
    steck_push(test_stack, (void*)expected);

    int16_t actual;
    ck_assert_int_eq(steck_pop(test_stack, (void*)&actual), true);

    ck_assert_int_eq(expected, actual);
}
END_TEST

START_TEST(test_stack_top_for_empty_returns_null)
{
    ck_assert_ptr_eq(steck_top(test_stack), NULL);
}
END_TEST

START_TEST(test_stack_top_returns_last)
{
    int16_t expected = 10;
    steck_push(test_stack, (void*)expected);

    int16_t actual = *(int16_t*)steck_top(test_stack);

    ck_assert_int_eq(expected, actual);
}
END_TEST

START_TEST(test_stack_count_for_empty_returns_zero)
{
    ck_assert_int_eq(steck_count(test_stack), 0);
}
END_TEST

START_TEST(test_stack_count_increased_after_push)
{
    size_t before = steck_count(test_stack);
    steck_push(test_stack, (void*)10);
    size_t after = steck_count(test_stack);

    ck_assert_int_eq(before + 1, after);
}
END_TEST

START_TEST(test_stack_count_decreased_after_pop)
{
    steck_push(test_stack, (void*)1);
    size_t before = steck_count(test_stack);

    int16_t item;
    steck_pop(test_stack, (void*)&item);

    size_t after = steck_count(test_stack);

    ck_assert_int_eq(before - 1, after);
}
END_TEST

static tcase_t* stack_tcase_new()
{
    tcase_t* tcase = tcase_create("test_stack_new");
    tcase_add_checked_fixture(tcase, setup_new, cleanup);

    tcase_add_test(tcase, test_stack_push);
    tcase_add_test(tcase, test_stack_pop_for_empty_returns_false);
    tcase_add_test(tcase, test_stack_pop_returns_last);
    tcase_add_test(tcase, test_stack_top_returns_last);
    tcase_add_test(tcase, test_stack_top_for_empty_returns_null);
    tcase_add_test(tcase, test_stack_count_increased_after_push);
    tcase_add_test(tcase, test_stack_count_decreased_after_pop);

    return tcase;
}

static tcase_t* stack_tcase_cleared()
{
    tcase_t* tcase = tcase_create("test_stack_cleared");
    tcase_add_checked_fixture(tcase, setup_cleared, cleanup);

    tcase_add_test(tcase, test_stack_push);
    tcase_add_test(tcase, test_stack_pop_for_empty_returns_false);
    tcase_add_test(tcase, test_stack_pop_returns_last);
    tcase_add_test(tcase, test_stack_top_returns_last);
    tcase_add_test(tcase, test_stack_top_for_empty_returns_null);
    tcase_add_test(tcase, test_stack_count_increased_after_push);
    tcase_add_test(tcase, test_stack_count_decreased_after_pop);

    return tcase;
}

static tcase_t* stack_tcase_reallocated_on_push()
{
    tcase_t* tcase = tcase_create("test_stack_reallocated_on_push");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_push, cleanup);

    tcase_add_test(tcase, test_stack_push);
    tcase_add_test(tcase, test_stack_pop_returns_last);
    tcase_add_test(tcase, test_stack_top_returns_last);
    tcase_add_test(tcase, test_stack_count_increased_after_push);
    tcase_add_test(tcase, test_stack_count_decreased_after_pop);

    return tcase;
}

static tcase_t* stack_tcase_reallocated_on_pop()
{
    tcase_t* tcase = tcase_create("test_stack_reallocated_on_pop");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_pop, cleanup);

    tcase_add_test(tcase, test_stack_push);
    tcase_add_test(tcase, test_stack_pop_returns_last);
    tcase_add_test(tcase, test_stack_top_returns_last);
    tcase_add_test(tcase, test_stack_count_increased_after_push);
    tcase_add_test(tcase, test_stack_count_decreased_after_pop);

    return tcase;
}

void add_stack_cases(suite_t* suite)
{
    suite_add_tcase(suite, stack_tcase_new());
    suite_add_tcase(suite, stack_tcase_cleared());
    suite_add_tcase(suite, stack_tcase_reallocated_on_push());
    suite_add_tcase(suite, stack_tcase_reallocated_on_pop());
}
