#include "test_queue.h"

#include <algc/queue.h>

static const size_t INITIAL_LENGTH = 4;
static const size_t SCALE_FACTOR = 2;

queue_t* test_queue;
int16_t firs_item;

static void setup_new()
{
    test_queue = queue_alloc(sizeof(int16_t));
}

static void setup_reallocated_on_push()
{
    setup_new();

    for (int16_t i = 0; i < INITIAL_LENGTH + 1; ++i)
    {
        queue_push(test_queue, (void*)(i + 1));
    }

    firs_item = 1;
}

static void setup_reallocated_on_pop()
{
    setup_reallocated_on_push();

    int16_t item;
    for (int i = 0; i < INITIAL_LENGTH / SCALE_FACTOR + 1; ++i)
    {
        queue_pop(test_queue, (void*)&item);
    }

    firs_item = 4;
}

static void setup_reversed()
{
    setup_new();

    for (int16_t i = 0; i < INITIAL_LENGTH; ++i)
    {
        queue_push(test_queue, (void*)(i + 1));
    }

    int16_t item;
    for (int i = 0; i < INITIAL_LENGTH / SCALE_FACTOR + 1; ++i)
    {
        queue_pop(test_queue, (void*)&item);
    }

    firs_item = 4;
}

static void setup_reallocated_reversed()
{
    setup_reversed();

    for (int16_t i = 0; i < INITIAL_LENGTH; ++i)
    {
        queue_push(test_queue, (void*)(i + 1));
    }

    firs_item = 4;
}

static void setup_cleared()
{
    setup_reallocated_reversed();

    int16_t item;
    while (queue_pop(test_queue, (void*)&item))
    {
    }
}

static void cleanup()
{
    queue_free(test_queue);
}

START_TEST(test_queue_push)
{
    queue_push(test_queue, (void*)10);
}
END_TEST

START_TEST(test_queue_pop_for_non_empty_returns_first)
{
    int16_t value;
    ck_assert_int_eq(queue_pop(test_queue, (void*)&value), true);
    ck_assert_int_eq(value, firs_item);
}
END_TEST

START_TEST(test_queue_pop_for_empty_returns_false)
{
    int16_t value;
    ck_assert_int_eq(queue_pop(test_queue, (void*)&value), false);
}
END_TEST

START_TEST(test_queue_top_for_non_empty_returns_first)
{
    int16_t value = *(int16_t*)queue_top(test_queue);
    ck_assert_int_eq(value, firs_item);
}
END_TEST

START_TEST(test_queue_top_for_empty_returns_null)
{
    ck_assert_ptr_eq(queue_top(test_queue), NULL);
}
END_TEST

START_TEST(test_queue_count_for_empty_returns_zero)
{
    ck_assert_int_eq(queue_count(test_queue), 0);
}
END_TEST

START_TEST(test_queue_count_increased_after_push)
{
    size_t before = queue_count(test_queue);
    queue_push(test_queue, (void*)10);
    size_t after = queue_count(test_queue);

    ck_assert_int_eq(before + 1, after);
}
END_TEST

START_TEST(test_queue_count_decreased_after_pop)
{
    queue_push(test_queue, (void*)10);
    size_t before = queue_count(test_queue);

    int16_t item;
    queue_pop(test_queue, (void*)&item);
    size_t after = queue_count(test_queue);

    ck_assert_int_eq(before - 1, after);
}
END_TEST

static tcase_t* queue_tcase_new()
{
    tcase_t* tcase = tcase_create("test_queue_new");
    tcase_add_checked_fixture(tcase, setup_new, cleanup);

    tcase_add_test(tcase, test_queue_push);
    tcase_add_test(tcase, test_queue_pop_for_empty_returns_false);
    tcase_add_test(tcase, test_queue_top_for_empty_returns_null);
    tcase_add_test(tcase, test_queue_count_for_empty_returns_zero);
    tcase_add_test(tcase, test_queue_count_increased_after_push);
    tcase_add_test(tcase, test_queue_count_decreased_after_pop);

    return tcase;
}

static tcase_t* queue_tcase_cleared()
{
    tcase_t* tcase = tcase_create("test_queue_cleared");
    tcase_add_checked_fixture(tcase, setup_cleared, cleanup);

    tcase_add_test(tcase, test_queue_push);
    tcase_add_test(tcase, test_queue_pop_for_empty_returns_false);
    tcase_add_test(tcase, test_queue_top_for_empty_returns_null);
    tcase_add_test(tcase, test_queue_count_for_empty_returns_zero);
    tcase_add_test(tcase, test_queue_count_increased_after_push);
    tcase_add_test(tcase, test_queue_count_decreased_after_pop);

    return tcase;
}

static tcase_t* queue_tcase_reallocated_on_push()
{
    tcase_t* tcase = tcase_create("test_queue_reallocated_on_push");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_push, cleanup);

    tcase_add_test(tcase, test_queue_push);
    tcase_add_test(tcase, test_queue_pop_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_top_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_count_increased_after_push);
    tcase_add_test(tcase, test_queue_count_decreased_after_pop);

    return tcase;
}

static tcase_t* queue_tcase_reallocated_on_pop()
{
    tcase_t* tcase = tcase_create("test_queue_reallocated_on_pop");
    tcase_add_checked_fixture(tcase, setup_reallocated_on_pop, cleanup);

    tcase_add_test(tcase, test_queue_push);
    tcase_add_test(tcase, test_queue_pop_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_top_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_count_increased_after_push);
    tcase_add_test(tcase, test_queue_count_decreased_after_pop);

    return tcase;
}

static tcase_t* queue_tcase_reallocated_reversed()
{
    tcase_t* tcase = tcase_create("test_queue_reallocated_reversed");
    tcase_add_checked_fixture(tcase, setup_reallocated_reversed, cleanup);

    tcase_add_test(tcase, test_queue_push);
    tcase_add_test(tcase, test_queue_pop_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_top_for_non_empty_returns_first);
    tcase_add_test(tcase, test_queue_count_increased_after_push);
    tcase_add_test(tcase, test_queue_count_decreased_after_pop);

    return tcase;
}

void add_queue_cases(suite_t* suite)
{
    suite_add_tcase(suite, queue_tcase_new());
    suite_add_tcase(suite, queue_tcase_cleared());
    suite_add_tcase(suite, queue_tcase_reallocated_on_push());
    suite_add_tcase(suite, queue_tcase_reallocated_on_pop());
    suite_add_tcase(suite, queue_tcase_reallocated_reversed());
}
