/**
 * Entry point for the unit tests for the library algc.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "common.h"
#include "test_heap.h"
#include "test_queue.h"
#include "test_stack.h"
#include "test_hashmap.h"

#define LOG_FILE_NAME "test_algc.log"

static suite_t* create_suite()
{
    suite_t* suite = suite_create("algc");

    add_stack_cases(suite);
    add_queue_cases(suite);
    add_heap_cases(suite);
    add_hashmap_cases(suite);

    return suite;
}

int main(void)
{
    srand(time(0));

    suite_t* suite = create_suite();

    srunner_t* runner = srunner_create(suite);
    srunner_set_xml(runner, LOG_FILE_NAME);
    // srunner_set_fork_status(runner, CK_NOFORK);
    srunner_run_all(runner, CK_VERBOSE);

    int number_failed = srunner_ntests_failed(runner);
    srunner_free(runner);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
