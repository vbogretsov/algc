/**
 * Test cases for <algc/stack.h>.
 * Copyright (c) 2017, Vladimir Bogretsov, Ilya Bogretsov
 * All rights reserved.
 */

#ifndef ALGC_TEST_QUEUE_H
#define ALGC_TEST_QUEUE_H

#include "common.h"

void add_queue_cases(suite_t* suite);

#endif // !ALGC_TEST_QUEUE_H