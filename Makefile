# UNIX
TEST_FILE		=	test
CC				= 	clang
LD 				= 	clang
CCFLAGS			= 	-std=c11 \
					-fcoverage-mapping \
					-fprofile-instr-generate \
					-Werror \
					-Wno-int-to-void-pointer-cast \
					-g
TEST_CCFLAGS	=	$(CCFLAGS) \
					-I/Users/vova/ports/include/ \
					-I./
LDFLAGS			=	-fcoverage-mapping \
					-fprofile-instr-generate
TEST_LDFLAGS	=	$(LDFLAGS) -L/Users/vova/ports/lib/ -lcheck

# Windows
# EXE_FILE		=	test.exe
# CC				= 	clang
# LD 				= 	clang
# CCFLAGS			= 	-std=c11 \
# 					-I"c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\include" \
# 					-I"c:\Program Files (x86)\Windows Kits\10\Include\10.0.14393.0\ucrt"
# TEST_CCFLAGS	=	$(CCFLAGS) \
# 					-I"c:\Program Files (x86)\check\include" \
# 					-I"src"
# LDFLAGS			=	-L"c:\Program Files (x86)\Windows Kits\10\Lib\10.0.14393.0\ucrt\x64" \
# 					-L"c:\Program Files (x86)\Windows Kits\10\Lib\10.0.14393.0\um\x64" \
# 					-L"c:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\lib\amd64"
# TEST_LDFLAGS	=	$(LDFLAGS) \
# 					-L"C:\Program Files (x86)\check\lib\" -lcheck

LIB_SRC = algc
LIB_OBJ = obj
LIB_SRC_FILES := $(wildcard $(LIB_SRC)/*.c)
LIB_OBJ_FILES := $(addprefix $(LIB_OBJ)/,$(notdir $(LIB_SRC_FILES:.c=.o)))

TEST_SRC = tests
TEST_OBJ = obj
TEST_SRC_FILES := $(wildcard $(TEST_SRC)/*.c)
TEST_OBJ_FILES := $(addprefix $(TEST_OBJ)/,$(notdir $(TEST_SRC_FILES:.c=.o)))

TEST_BIN = bin
TEST_BIN_FILE = $(TEST_BIN)/$(TEST_FILE)

default: $(TEST_BIN) $(TEST_BIN_FILE)

clean:
	@echo cleaning...
	rm -rf $(LIB_OBJ)
	rm -rf $(TEST_OBJ)
	rm -rf $(TEST_BIN)

test: default
	$(TEST_BIN_FILE)

$(LIB_OBJ):
	mkdir $(LIB_OBJ)

$(TEST_BIN):
	mkdir $(TEST_BIN)

$(LIB_OBJ)/%.o: $(LIB_SRC)/%.c $(LIB_OBJ)
	$(CC) $(CCFLAGS) -c $< -o $@

$(TEST_OBJ)/%.o: $(TEST_SRC)/%.c $(TEST_OBJ)
	$(CC) $(TEST_CCFLAGS) -c $< -o $@

$(TEST_BIN_FILE): $(LIB_OBJ_FILES) $(TEST_OBJ_FILES)
	$(LD) $(TEST_LDFLAGS) -o $@ $^
